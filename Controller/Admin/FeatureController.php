<?php


namespace Comsa\SuluFeatureListBundle\Controller\Admin;


use Comsa\SuluFeatureListBundle\Entity\Feature;
use Comsa\SuluFeatureListBundle\Repository\FeatureRepository;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\View\ViewHandlerInterface;
use Sulu\Component\Rest\AbstractRestController;
use Sulu\Component\Rest\ListBuilder\AbstractListBuilder;
use Sulu\Component\Rest\ListBuilder\Doctrine\DoctrineListBuilderFactoryInterface;
use Sulu\Component\Rest\ListBuilder\ListRepresentation;
use Sulu\Component\Rest\ListBuilder\ListRestHelperInterface;
use Sulu\Component\Rest\ListBuilder\Metadata\FieldDescriptorFactoryInterface;
use Sulu\Component\Rest\RestHelperInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class FeatureController extends AbstractRestController implements ClassResourceInterface
{
    private $listRestHelper;
    private $factory;
    private $restHelper;
    private $doctrineListBuilderFactory;
    private $fieldDescriptorFactory;
    private $repository;

    public function __construct(
        ViewHandlerInterface $viewHandler,
        ListRestHelperInterface $listRestHelper,
        DoctrineListBuilderFactoryInterface $doctrineListBuilderFactory,
        RestHelperInterface $restHelper,
        FieldDescriptorFactoryInterface $fieldDescriptorFactory,
        EntityManagerInterface $entityManager
    )
    {
        $this->listRestHelper = $listRestHelper;
        $this->factory = $doctrineListBuilderFactory;
        $this->restHelper = $restHelper;
        $this->fieldDescriptorFactory = $fieldDescriptorFactory;
        $this->repository = $entityManager->getRepository(Feature::class);
        parent::__construct($viewHandler);
    }

    public function getModelClass(): string
    {
        return Feature::class;
    }

    public function getListName(): string
    {
        return 'features';
    }

    public function cgetAction(Request $request): Response
    {
        $locale = $this->getLocale($request);

        if (!$locale) {
            $locale = 'nl';
        }

        $listBuilder = $this->factory->create($this->getModelClass());

        $fieldDescriptors = $this->fieldDescriptorFactory->getFieldDescriptors(Feature::RESOURCE_KEY);

        $this->restHelper->initializeListBuilder($listBuilder, $fieldDescriptors);

        $listBuilder->setParameter('locale', $locale);
        $listBuilder->setParameter('defaultLocale', 'nl');

        // load entities
        $list = $listBuilder->execute();

        // get pagination
        $total = $listBuilder->count();
        $page = $listBuilder->getCurrentPage();
        $limit = $listBuilder->getLimit();

        // create list representation
        $representation = new ListRepresentation(
            $list,
            $this->getListName(),
            $request->get('_route'),
            $request->query->all(),
            $page,
            $limit,
            $total
        );

        return $this->handleView($this->view($representation));
    }

    public function getAction(int $id, Request $request): Response
    {
        $entity = $this->load($id, $request);
        if (!$entity) {
            throw new NotFoundHttpException();
        }

        return $this->handleView($this->view($entity));
    }

    public function postAction(Request $request): Response
    {
        $entity = $this->create($request);
        $this->mapDataToEntity($request->request->all(), $entity);
        $this->save($entity);
        return $this->handleView($this->view($entity));
    }

    public function putAction(int $id, Request $request): Response
    {
        $entity = $this->load($id, $request);
        if (!$entity) {
            throw new NotFoundHttpException();
        }

        $this->mapDataToEntity($request->request->all(), $entity);

        $this->save($entity);

        return $this->handleView($this->view($entity));
    }

    public function deleteAction(int $id): Response
    {
        $this->remove($id);

        return $this->handleView($this->view());
    }

    protected function mapDataToEntity(array $data, Feature $entity): void
    {
        $entity->setTitle($data['title']);
    }

    protected function load(int $id, Request $request): ?Feature
    {
        return $this->repository->findById($id, $request->query->get('locale'));
    }

    protected function save(Feature $entity): void
    {
        $this->repository->save($entity);
    }

    protected function create(Request $request): Feature
    {
        return $this->repository->create($request->query->get('locale'));
    }

    protected function remove(int $id): void
    {
        $this->repository->remove($id);
    }
}
