Installation
============

Make sure Composer is installed globally, as explained in the
[installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

### Step 1: Download the Bundle

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```console
$ composer require comsa/sulu-feature-list-bundle
```

### Step 2: Enable the Bundle

Then, enable the bundle by adding it to the list of registered bundles
in the `config/bundles.php` file of your project:

```php
// config/bundles.php

return [
    // ...
    Comsa\SuluFeatureListBundle::class => ['all' => true],
];
```

### Step 3: Add JS to webpack build process

In `assets/admin/package.json` add the feature list dependencies like so:

```json
{
  // ...
    "dependencies": {
        // ...
        "comsa-sulu-feature-list-bundle": "file:../../vendor/comsa/sulu-feature-list-bundle/Resources/js"
    }
}
```
Install these assets using `npm install`

### Step 4: Add Feature list bundle package to admin panel
In `assets/admin/index.js` add the following above `startAdmin()`:

```js
// ...
// Implement custom extensions here
import 'comsa-sulu-feature-list-bundle';
// ...
```

Usage
=====

You can now use this is a normal content type:

```
<property name="features" type="feature_list">
    <meta>
        <title lang="en">Feature list</title>
    </meta>
</property>
```
