<?php


namespace Comsa\SuluFeatureListBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="comsa_feature_translation")
 */
class FeatureTranslation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Comsa\SuluFeatureListBundle\Entity\Feature", inversedBy="translations")
     * @ORM\JoinColumn(name="feature_id", onDelete="cascade", referencedColumnName="id")
     */
    private $feature;

    /**
     * @ORM\Column(type="string")
     */
    private $locale;

    /**
     * @ORM\Column(type="string")
     */
    private $title;

    public function __construct(Feature $feature, string $locale)
    {
        $this->feature = $feature;
        $this->locale = $locale;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getLocale()
    {
        return $this->locale;
    }

    public function getFeature()
    {
        return $this->feature;
    }

    public function setTitle($title): void
    {
        $this->title = $title;
    }

    public function setLocale($locale): void
    {
        $this->locale = $locale;
    }

    public function setFeature($feature): void
    {
        $this->feature = $feature;
    }
}
