<?php

namespace Comsa\SuluFeatureListBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="Comsa\SuluFeatureListBundle\Repository\FeatureRepository")
 * @ORM\Table(name="comsa_feature")
 */
class Feature
{
    const RESOURCE_KEY = 'features';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    private $defaultLocale = 'nl';

    /**
     * @var Collection|FeatureTranslation[]
     * @ORM\OneToMany(targetEntity="Comsa\SuluFeatureListBundle\Entity\FeatureTranslation", mappedBy="feature", cascade={"all"})
     */
    private $translations;

    /**
     * @var string $locale
     */
    private $locale;

    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    public function getTranslations()
    {
        return $this->translations;
    }

    public function getTranslation(string $locale, bool $fallback = false): ?FeatureTranslation
    {
        foreach ($this->translations as $translation) {
            if ($translation->getLocale() == $locale) {
                return $translation;
            }
        }

        if ($fallback) {
            return $this->getTranslation($this->getDefaultLocale());
        }

        return null;
    }

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function setLocale(string $locale): void
    {
        $this->locale = $locale;
    }

    /**
     * @Serializer\VirtualProperty(name="title")
     */
    public function getTitle()
    {
        return $this->getTranslation($this->locale, true)->getTitle();
    }

    public function setTitle($title)
    {
        $translation = $this->getTranslation($this->locale);
        if (!$translation) {
            $translation = $this->createTranslation($this->locale);
        }

        $translation->setTitle($title);

        return $this;
    }

    protected function createTranslation(string $locale): FeatureTranslation
    {
        $translation = new FeatureTranslation($this, $locale);
        $this->translations->set($locale, $translation);

        return $translation;
    }

    public function getDefaultLocale(): string
    {
        return $this->defaultLocale;
    }

    /**
     * @Serializer\VirtualProperty(name="default_title")
     */
    public function getDefaultTitle() {
        return $this->getTranslation($this->getDefaultLocale())->getTitle();
    }
}
