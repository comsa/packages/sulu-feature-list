<?php

namespace Comsa\SuluFeatureListBundle\Repository;

use Comsa\SuluFeatureListBundle\Entity\Feature;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class FeatureRepository extends EntityRepository
{
    public function create(string $locale): Feature
    {
        $feature = new Feature();
        $feature->setLocale($locale);
        return $feature;
    }

    public function findById(int $id, string $locale): ?Feature
    {
        $feature = $this->find($id);
        if (!$feature) {
            return null;
        }

        $feature->setLocale($locale);

        return $feature;
    }

    public function remove(int $id): void
    {
        $this->getEntityManager()->remove(
            $this->getEntityManager()->getReference(
                $this->getClassName(),
                $id
            )
        );
        $this->getEntityManager()->flush();
    }

    public function save(Feature $feature): void
    {
        $this->getEntityManager()->persist($feature);
        $this->getEntityManager()->flush();
    }
}
