<?php


namespace Comsa\SuluFeatureListBundle\Content\Types;


use Comsa\SuluFeatureListBundle\Entity\Feature;
use Comsa\SuluFeatureListBundle\Repository\FeatureRepository;
use Doctrine\ORM\EntityManagerInterface;
use PHPCR\NodeInterface;
use Sulu\Component\Content\Compat\PropertyInterface;
use Sulu\Component\Content\ComplexContentType;

class FeatureSelection extends ComplexContentType
{
    private $featureRepository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->featureRepository = $entityManager->getRepository(Feature::class);
    }

    public function read(NodeInterface $node, PropertyInterface $property, $webspaceKey, $languageCode, $segmentKey)
    {
        $featureIds = $node->getPropertyValueWithDefault($property->getName(), []);
        $property->setValue($featureIds);
    }

    public function write(
        NodeInterface $node,
        PropertyInterface $property,
        $userId,
        $webspaceKey,
        $languageCode,
        $segmentKey
    ) {
        $featureIds = [];
        $value = $property->getValue();

        if (null === $value) {
            $node->setProperty($property->getName(), null);

            return;
        }

        foreach ($value as $feature) {
            if (is_numeric($feature)) {
                // int value for id
                $featureIds[] = $feature;
            } else {
                // full category object use only id to save
                $featureIds[] = $feature['id'];
            }
        }

        $node->setProperty($property->getName(), $featureIds);
    }

    public function remove(NodeInterface $node, PropertyInterface $property, $webspaceKey, $languageCode, $segmentKey)
    {
        if ($node->hasProperty($property->getName())) {
            $property = $node->getProperty($property->getName());
            $property->remove();
        }
    }

    public function getContentData(PropertyInterface $property)
    {
        $features = $property->getValue();
        foreach ($features as &$feature) {
            $feature = $this->featureRepository->findById($feature, $property->getStructure()->getLanguageCode());
        }
        return $features;
    }
}
