<?php


namespace Comsa\SuluFeatureListBundle\Content\Types;

use Comsa\SuluFeatureListBundle\Entity\Feature;
use Doctrine\ORM\EntityManagerInterface;
use PHPCR\NodeInterface;
use Sulu\Component\Content\Compat\PropertyInterface;
use Sulu\Component\Content\SimpleContentType;

class FeatureList extends SimpleContentType
{
    private $featureRepository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->featureRepository = $em->getRepository(Feature::class);
        parent::__construct('FeatureList', '');
    }

    public function read(NodeInterface $node, PropertyInterface $property, $webspaceKey, $languageCode, $segmentKey)
    {
        $value = $this->defaultValue;
        if ($node->hasProperty($property->getName())) {
            $value = json_decode($node->getPropertyValue($property->getName()), true);
        }

        $property->setValue($this->decodeValue($value));

        return $value;
    }

    public function write(NodeInterface $node, PropertyInterface $property, $userId, $webspaceKey, $languageCode, $segmentKey)
    {
        $allowedProperties = [
            'linkedFeature', 'value', 'important'
        ];
        $features = $property->getValue();
        foreach ($features as $key => $feature) {
            foreach ($feature as $prop => $value) {
                if (!in_array($prop, $allowedProperties)) {
                    unset($features[$key][$prop]);
                }
            }
        }

        $property->setValue(json_encode($features));
        parent::write($node, $property, $userId, $webspaceKey, $languageCode, $segmentKey);
    }

    public function getContentData(PropertyInterface $property)
    {
        $features = $property->getValue();
        foreach ($features as &$feature) {
            $feature['linkedFeature'] = $this->featureRepository->findById((int) $feature['linkedFeature'], $property->getStructure()->getLanguageCode());
        }
        return $features;
    }
}
