import React from 'react';
import {fieldRegistry} from 'sulu-admin-bundle/containers';
import { ReactSortable } from "react-sortablejs";
import Button from 'sulu-admin-bundle/components/Button'
import Input from 'sulu-admin-bundle/components/Input'
import Checkbox from 'sulu-admin-bundle/components/Checkbox'
import Grid from 'sulu-admin-bundle/components/Grid'
import SingleSelect from 'sulu-admin-bundle/components/SingleSelect'
import Field from 'sulu-admin-bundle/components/Form/Field'
import Loader from 'sulu-admin-bundle/components/Loader'
import {ResourceRequester} from 'sulu-admin-bundle/services';
const Option = SingleSelect.Option;

// @flow
import { listFieldTransformerRegistry } from 'sulu-admin-bundle/containers';
import FeatureKeywordsMultipleUsageTransformer
  from './containers/List/fieldTransformers/FeatureKeywordsMultipleUsageTransformer';

listFieldTransformerRegistry.add('feature_keywords_multiple_usage', new FeatureKeywordsMultipleUsageTransformer());


class FeatureList extends React.Component {
  constructor(props) {
    super(props);
    const { value } = this.props
    this.state = {
      feature: {
        linkedFeature: '',
        value: '',
        important: false
      },
      features: value ? value : [],
      featureList: []
    }
    this.handleChange = this.handleChange.bind(this);
    this.addFeature = this.addFeature.bind(this);

    ResourceRequester.getList('features', {
      limit: 200
    }).then(response => {
      this.setState({
        featureList: response['_embedded'].features
      });
    })
  }
  addFeature() {
    if (this.state.feature.value.length) {
      let features = this.state.features.concat(this.state.feature);
      this.setState({
        features,
        feature: {
          linkedFeature: '',
          value: '',
          important: false
        }
      });
      const { onChange } = this.props;
      onChange(features);
    }
  }
  removeFeature(index) {
    let features = this.state.features.slice();
    features.splice(index, 1);
    this.setState({
      features
    })
    const { onChange } = this.props;
    onChange(features);
  }

  render() {
    return (
      <div>
        <ReactSortable list={this.state.features} setList={newState => {
          this.setState({ features: newState })
          const { onChange } = this.props;
          onChange(newState);
        }}>
          {this.state.features.map((feature, index) => (
            <Grid key={index}>
              <Field label="Titel" colSpan={3}>
                {this.state.featureList.length > 0 ? <SingleSelect value={feature.linkedFeature} onChange={(v) => this.handleChange(v, 'linkedFeature', index)}>
                  {this.state.featureList.map((featureListItem, index) => {
                    return <Option value={featureListItem.id} key={index}>{featureListItem.title}</Option>
                  })}
                </SingleSelect> : <Loader />}
              </Field>
              <Field label="Waarde" colSpan={3}>
                <Input type="text" value={feature.value} onChange={(v) => this.handleChange(v, 'value', index)}/>
              </Field>
              <Field label="Belangrijk" colSpan={2}>
                <Checkbox onChange={(v) => this.handleChange(v, 'important', index)} type="checkbox" checked={feature.important}/>
              </Field>
              <Field colSpan={4}>
                <Button icon="su-minus" onClick={() => this.removeFeature(index)}>Verwijderen</Button>
              </Field>
            </Grid>
          ))}
        </ReactSortable>
        <hr/>
        <Grid>
          <Field label="Titel" colSpan={3}>
            {this.state.featureList.length > 0 ? <SingleSelect value={this.state.feature.linkedFeature} onChange={(v) => this.handleChange(v, 'linkedFeature')}>
              {this.state.featureList.map((feature, index) => {
                return <Option value={feature.id} key={index}>{feature.title}</Option>
              })}
            </SingleSelect> : <Loader />}
          </Field>
          <Field label="Waarde" colSpan={3}>
            <Input onChange={(v) => this.handleChange(v, 'value')} value={this.state.feature.value}/>
          </Field>
          <Field label="Belangrijk" colSpan={2}>
            <Checkbox onChange={(v) => this.handleChange(v, 'important')} checked={this.state.feature.important} />
          </Field>
          <Field colSpan={4}>
            <Button icon="su-plus" onClick={this.addFeature}>Voeg toe</Button>
          </Field>
        </Grid>
      </div>
    );
  }

  handleChange(value, name, i) {
    if (i !== undefined) {
      let features = this.state.features.slice();
      features[i] = {
        ...this.state.features[i],
        [name]: value
      };
      this.setState({
        features
      })
    } else {
      this.setState({
        feature: {
          ...this.state.feature,
          [name]: value
        }
      });
    }
  }
}
fieldRegistry.add(
  'feature_list', FeatureList
);
