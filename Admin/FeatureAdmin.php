<?php

namespace Comsa\SuluFeatureListBundle\Admin;

use Comsa\SuluFeatureListBundle\Entity\Feature;
use Sulu\Bundle\AdminBundle\Admin\Admin;
use Sulu\Bundle\AdminBundle\Admin\Navigation\NavigationItemCollection;
use Sulu\Bundle\AdminBundle\Admin\View\TogglerToolbarAction;
use Sulu\Bundle\AdminBundle\Admin\View\ToolbarAction;
use Sulu\Bundle\AdminBundle\Admin\View\ViewBuilderFactoryInterface;
use Sulu\Bundle\AdminBundle\Admin\View\ViewCollection;
use Sulu\Bundle\WebsiteBundle\Navigation\NavigationItem;
use Sulu\Component\Webspace\Manager\WebspaceManagerInterface;

class FeatureAdmin extends Admin
{
    const FEATURE_LIST_KEY = 'features';
    const FEATURE_LIST_VIEW = 'comsa.sulu_feature_list.features_list';
    const FEATURE_ADD_VIEW = 'comsa.sulu_feature_list.features_add';
    const FEATURE_EDIT_VIEW = 'comsa.sulu_feature_list.features_edit';
    const FEATURE_FORM_KEY = 'feature_details';

    private $viewBuilderFactory;
    private $webspaceManager;

    public function __construct(
        ViewBuilderFactoryInterface $viewBuilderFactory,
        WebspaceManagerInterface $webspaceManager
    ) {
        $this->viewBuilderFactory = $viewBuilderFactory;
        $this->webspaceManager = $webspaceManager;
    }

    public function configureNavigationItems(NavigationItemCollection $navigationItemCollection): void
    {
        $navigationItem = new \Sulu\Bundle\AdminBundle\Admin\Navigation\NavigationItem('comsa.feature_list.features');
        $navigationItem->setIcon('fa-list');
        $navigationItem->setPosition(10);
        $navigationItem->setView(static::FEATURE_LIST_VIEW);

        $navigationItemCollection->add($navigationItem);
    }

    public function configureViews(ViewCollection $viewCollection): void
    {
        $locales = $this->webspaceManager->getAllLocales();

        //-- List view
        $listToolbarActions = [new ToolbarAction('sulu_admin.add'), new ToolbarAction('sulu_admin.delete')];
        $listView = $this->viewBuilderFactory->createListViewBuilder(self::FEATURE_LIST_VIEW, '/features/:locale')
            ->setResourceKey(Feature::RESOURCE_KEY)
            ->setListKey(self::FEATURE_LIST_KEY)
            ->setTitle('comsa.feature_list.features')
            ->addListAdapters(['table'])
            ->addLocales($locales)
            ->setDefaultLocale($locales[0])
            ->setAddView(static::FEATURE_ADD_VIEW)
            ->setEditView(static::FEATURE_EDIT_VIEW)
            ->addToolbarActions($listToolbarActions);

        //-- Add view
        $viewCollection->add(
            $this->viewBuilderFactory->createResourceTabViewBuilder(static::FEATURE_ADD_VIEW, '/features/:locale/add')
                ->setResourceKey(Feature::RESOURCE_KEY)
                ->addLocales($locales)
                ->setBackView(static::FEATURE_LIST_VIEW)
        );

        $viewCollection->add(
            $this->viewBuilderFactory->createFormViewBuilder(static::FEATURE_ADD_VIEW . '.details', '/details')
                ->setResourceKey(Feature::RESOURCE_KEY)
                ->setFormKey(static::FEATURE_FORM_KEY)
                ->setTabTitle('comsa.feature_list.general')
                ->setEditView(static::FEATURE_EDIT_VIEW)
                ->addToolbarActions([
                    new ToolbarAction('sulu_admin.save')
                ])
                ->setParent(static::FEATURE_ADD_VIEW)
        );

        $formToolbarActions = [
            new ToolbarAction('sulu_admin.save'),
            new ToolbarAction('sulu_admin.delete')
        ];
        //-- Edit view
        $viewCollection->add($this->viewBuilderFactory->createResourceTabViewBuilder(static::FEATURE_EDIT_VIEW, '/features/:locale/:id')
            ->setResourceKey(Feature::RESOURCE_KEY)
            ->setBackView(static::FEATURE_LIST_VIEW)
            ->setTitleProperty('title')
            ->addLocales($locales)
        );
        $viewCollection->add(
            $this->viewBuilderFactory->createFormViewBuilder(static::FEATURE_EDIT_VIEW . '.details', '/details')
                ->setResourceKey(Feature::RESOURCE_KEY)
                ->setFormKey(self::FEATURE_FORM_KEY)
                ->setTabTitle('sulu_admin.details')
                ->addToolbarActions($formToolbarActions)
                ->setParent(static::FEATURE_EDIT_VIEW)
        );

        $viewCollection->add($listView);
    }
}
