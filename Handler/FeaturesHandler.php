<?php
namespace Comsa\SuluFeatureListBundle\Handler;

use Comsa\SuluFeatureListBundle\Entity\Feature;
use Comsa\SuluPageExport\Handler\BaseHandler;
use Doctrine\ORM\EntityManagerInterface;
use Sulu\Bundle\MediaBundle\Media\Manager\MediaManagerInterface;
use Sulu\Bundle\PageBundle\Document\BasePageDocument;

class FeaturesHandler extends BaseHandler
{
    const FIELD_NAMES = ['features'];

    private $entityManager;

    public function __construct(MediaManagerInterface $mediaManager, EntityManagerInterface $entityManager)
    {
        parent::__construct($mediaManager);
        $this->entityManager = $entityManager;
    }

    public function handleData(BasePageDocument $document): array
    {
        $structure = $document->getStructure()->toArray();

        $data = [];
        foreach (self::FIELD_NAMES as $FIELD_NAME) {
            if(!isset($structure[$FIELD_NAME])) continue;
            if (!is_array($structure[$FIELD_NAME])) continue;
            $data = array_merge($data, $this->handleFeatures($structure[$FIELD_NAME], $document->getLocale()));
        }
        return $data;
    }

    private function handleFeatures(array $features, string $locale): array
    {
        $repository = $this->entityManager->getRepository(Feature::class);

        $data = [];
        foreach ($features as $feature) {
            $featureObject = $repository->find($feature['linkedFeature']);
            $data['features_' . strtolower($featureObject->getTranslation($locale)->getTitle())] = $feature['value'];
        }
        return $data;
    }
}
