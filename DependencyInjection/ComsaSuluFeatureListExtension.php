<?php


namespace Comsa\SuluFeatureListBundle\DependencyInjection;


use Comsa\SuluFeatureListBundle\Handler\FeaturesHandler;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class ComsaSuluFeatureListExtension extends Extension implements PrependExtensionInterface
{
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yaml');
        $loader->load('content_types.yaml');
    }

    public function prepend(ContainerBuilder $container)
    {
        if ($container->hasExtension('fos_js_routing')) {
            $container->prependExtensionConfig(
                'fos_js_routing',
                [
                    'routes_to_expose' => [
                        'comsa.feature_list.get_features',
                        'comsa.feature_list.get_feature',
                    ],
                ]
            );
        }

        if ($container->hasExtension('sulu_admin')) {
            $container->prependExtensionConfig(
                'sulu_admin',
                [
                    'lists' => [
                        'directories' => [
                            __DIR__ . '/../Resources/config/lists',
                        ],
                    ],
                    'forms' => [
                        'directories' => [
                            __DIR__ . '/../Resources/config/forms',
                        ],
                    ],
                    'resources' => [
                        'features' => [
                            'routes' => [
                                'list' => 'comsa.feature_list.get_features',
                                'detail' => 'comsa.feature_list.get_feature'
                            ]
                        ]
                    ],
                    'field_type_options' => [
                        'selection' => [
                            'feature_selection' => [
                                'default_type' => 'list_overlay',
                                'resource_key' => 'features',
                                'types' => [
                                    'list_overlay' => [
                                        'adapter' => 'table',
                                        'list_key' => 'features',
                                        'display_properties' => [
                                            'title'
                                        ],
                                        'icon' => 'su-house',
                                        'label' => 'Eigenschappen',
                                        'overlay_title' => 'Selecteer uw eigenschappen'
                                    ],
                                ],
                            ]
                        ]
                    ]
                ]
            );
        }

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
    }
}
